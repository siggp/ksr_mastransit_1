﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Subskrybent
{
    class MyConsumer : DefaultBasicConsumer
    {
        public MyConsumer(IModel model) : base(model) { }
        public override void HandleBasicDeliver(string consumerTag, ulong deliveryTag, bool
            redelivered, string exchange, string routingKey, IBasicProperties properties, byte[] body)
        {
            var message = Encoding.UTF8.GetString(body);
            Console.WriteLine(message);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory()
            {
                UserName = "xtgdkfmw",
                Password = "htUtcmGXLGbe1ZkCH3mj6U8JHHb8XWLf",
                HostName = "llama-01.rmq.cloudamqp.com",
                VirtualHost = "xtgdkfmw"
            };

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare("head_rout", "headers");
                var queueName = channel.QueueDeclare().QueueName;
                var headers = new Dictionary<string, object>{
                    { "x-match", "all" }, //any or all
                    { "part", "first" },
                    { "number", 42 }
                };
                channel.QueueBind(queueName, "head_rout", "", headers);

                var consumer = new MyConsumer(channel);
                while (true)
                {
                    channel.BasicConsume(queueName, false, consumer);
                }
            }
        }
    }
}
