﻿using Contract;
using RabbitMQ.Client;
using RabbitMQ.ServiceModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WCF_Server
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceHost serviceHost = new ServiceHost(
                typeof(Calculator),
                new Uri[]
                {
                    //new Uri("http://localhost:8800"),
                    new Uri("soap.amqp:///")
            });

            serviceHost.AddServiceEndpoint(
                typeof(ICalculator),
                new RabbitMQBinding(
                    "llama-01.rmq.cloudamqp.com",
                    AmqpTcpEndpoint.UseDefaultPort,
                    "xtgdkfmw",
                    "htUtcmGXLGbe1ZkCH3mj6U8JHHb8XWLf",
                    "xtgdkfmw",
                    8192,
                    Protocols.AMQP_0_9_1
                ),
                "net.msmq://llama-01.rmq.cloudamqp.com"
            );
            serviceHost.Open();
            Console.ReadKey();
        }
    }
}
