﻿using RabbitMQ.Client;
using RabbitMQ.Client.Framing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wydawca
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory()
            {
                UserName = "xtgdkfmw",
                Password = "htUtcmGXLGbe1ZkCH3mj6U8JHHb8XWLf",
                HostName = "llama-01.rmq.cloudamqp.com",
                VirtualHost = "xtgdkfmw"
            };

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                BasicProperties props = new BasicProperties();
                props.Headers = new Dictionary<string, object>{
                    { "part", "first" },
                    { "number", 42 }
                };
                channel.ExchangeDeclare("head_rout", "headers");

                string message = "Wiadomosc :-)";
                channel.BasicPublish(
                    "head_rout",
                    "",
                    props,
                    Encoding.UTF8.GetBytes(message)
                );
            }
        }
    }
}
