﻿using Contract;
using RabbitMQ.Client;
using RabbitMQ.ServiceModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WCF_Client
{
    class Program
    {
        static void Main(string[] args)
        {
            var fact = new ChannelFactory<ICalculator>(
                new RabbitMQBinding(
                    "llama-01.rmq.cloudamqp.com",
                    AmqpTcpEndpoint.UseDefaultPort,
                    "xtgdkfmw",
                    "htUtcmGXLGbe1ZkCH3mj6U8JHHb8XWLf",
                    "xtgdkfmw",
                    8192,
                    Protocols.AMQP_0_9_1
                ),
                new EndpointAddress("net.msmq://llama-01.rmq.cloudamqp.com")
            );

            var client = fact.CreateChannel();
            var x = client.Add(3, 5);
            Console.WriteLine(x);

            Console.ReadKey();
        }
    }
}
