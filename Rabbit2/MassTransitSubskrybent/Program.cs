﻿using Contract;
using MassTransit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MassTransitSubskrybent
{
    class Program
    {
        public static Task Handle(ConsumeContext<Communicate> ctx)
        {
            return Console.Out.WriteLineAsync($"received: {ctx.Message.Message}");
        }

        static void Main(string[] args)
        {
            var bus = Bus.Factory.CreateUsingRabbitMq(otions => {
                var host = otions.Host(
                    new Uri("rabbitmq://llama.rmq.cloudamqp.com/xtgdkfmw"),
                h => {
                    h.Username("xtgdkfmw");
                    h.Password("htUtcmGXLGbe1ZkCH3mj6U8JHHb8XWLf");
                });
                otions.ReceiveEndpoint(host, "recvqueue", ep => {
                    ep.Handler<Communicate>(Handle);
                });
            });

            bus.Start();
            Console.WriteLine("odbiorca wystartował");
            Console.ReadKey();
            bus.Stop();
        }
    }
}
